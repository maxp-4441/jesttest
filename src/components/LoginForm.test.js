import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Button  from './common/Button';
import Card from './common/Card'
import renderer from 'react-test-renderer';
import LoginForm from './LoginForm';
import CardSection from './common/CardSection';
import Input from './common/Input';
import App from '../App';
import toJson from 'enzyme-to-json'; //added this line

configure({adapter: new Adapter()});

describe('<Card/>', () => {
    describe('Rendering', () => {
        beforeEach(() => {
            jest.useFakeTimers();
          });
        it('should match to our Card snapshot', () => {
            const component = shallow( <Card/>);
            expect(toJson(component)).toMatchSnapshot();
        });
    });
});

describe('<Cardsection/>', () => {
    describe('Rendering', () => {
        beforeEach(() => {
            jest.useFakeTimers();
          });
        it('should match to our Cardsection snapshot', () => {
            const component = shallow(<CardSection /> );
            expect(toJson(component)).toMatchSnapshot();
        });
    });
});

describe('<Input/>', () => {
    describe('Rendering', () => {
        beforeEach(() => {
            jest.useFakeTimers();
          });
        it('should match to our Input snapshot', () => {
            const component = shallow(<Input /> );
            expect(toJson(component)).toMatchSnapshot();
        });
    });
});

// describe('<Button/>', () => {
//     describe('Rendering', () => {
//         it('should match to our button snapshot', () => {
//             const component = shallow(<Button children="Log In" /> );
//             expect(toJson(component)).toMatchSnapshot();
//         });
//     });
// });
    
// let findElement = (component, element) => {
//     let r = undefined;
//     for( node in component.children){
//         if(component.children[node].props.testID == 'cardsection'){
//             if(component.children[node].children[0].props.testID == 'login'){
//                 r = component.children[node].children[0].props.testID

//             }
//         } 
//     }
//     return r;

// }
// describe('Find Element', async () => {
//     it('Log In button', () => {
//         const component = renderer.create( <LoginForm />).toJSON();
//         expect(await findElement(component,'login')).toBeDefined()
//     });
// });
describe('App', () => {
    beforeAll(() => {
      app = shallow(<App />);
    //  console.warn(app.state())
    });
    beforeEach(() => {
        jest.useFakeTimers();
      });
    it('App renders nested components', () => {
        expect(app.find('Header').length).toEqual(1);
      });
    it('username check', () => {
        const wrapper = renderer.create(<LoginForm />).getInstance();
        wrapper.handleMail('admin@admin.com')
      //  console.warn(wrapper.state)
        expect(wrapper.state.email).toEqual('admin@admin.com');
    
      });
      it('password check', () => {
        const wrapper = renderer.create(<LoginForm />).getInstance();
        wrapper.handlePassword('admin123')
     //   console.warn(wrapper.state);
        expect(wrapper.state.password).toEqual('admin123');
      });
      it('login check with wrong data',()=>{
        const wrapper1 = renderer.create(<LoginForm />).getInstance();
        wrapper1.handleMail('admin1@admin.com')
        wrapper1.handlePassword('admin123')
        wrapper1.onButtonPress()
       // console.warn(wrapper1.state)
        });
});
