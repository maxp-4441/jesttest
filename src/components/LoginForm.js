import React from 'react';
import { Text } from 'react-native';
import firebase from 'firebase';
import Button  from './common/Button';
import Spinner from './common/Spinner';
import Card from './common/Card';
import CardSection from './common/CardSection';
import Input from './common/Input';
import Users from './Users';

export default class LoginForm extends React.Component {
  state = { email: '', password: '', error: '', loading: false };

  // componentDidMount(){
  //   Users.all().then((data) => {
  //     console.warn(data)
  //   })
  // }
  

  onButtonPress() {
    const { email, password } = this.state;

    this.setState({ error: '', loading: true });

    firebase.auth().signInWithEmailAndPassword(email, password)
      .then(this.onLoginSuccess.bind(this))
      .catch(() => {
        firebase.auth().createUserWithEmailAndPassword(email, password)
          .then(this.onLoginSuccess.bind(this))
          .catch(this.onLoginFail.bind(this));
      });
  }

  onLoginFail() {
    this.setState({ error: 'Authentication Failed', loading: false });
  }

  onLoginSuccess() {
    this.setState({
      email: '',
      password: '',
      loading: false,
      error: ''
    });
  }


  renderButton() {
    if (this.state.loading) {
      return <Spinner size="small" />;
    }

    return (
      <Button 
      testID={'login'}
      onPress={() => this.onButtonPress()}>
        Log in
      </Button>
    );
  }

  handleMail = (e) => {
    this.setState({ email :e })
  }

  handlePassword = (e) => {
    this.setState({ password: e })
  }

  render() {
    return (
      <Card>
        <CardSection testID={'cardsection'}>
          <Input
          testID={'username'}
            placeholder="user@gmail.com"
            label="Email"
            value={this.state.email}
            onChangeText={(e) => this.handleMail(e)}
          />
        </CardSection>

        <CardSection testID={'cardsection'}>
          <Input
            testID={'password'}
            secureTextEntry
            placeholder="password"
            label="Password"
            value={this.state.password}
            onChangeText={(e) => this.handlePassword(e)}
          />
        </CardSection>

        <Text 
        testID={'error'}
        style={styles.errorTextStyle}>
          {this.state.error}
        </Text>

        <CardSection testID={'cardsection'}>
          {this.renderButton()}
        </CardSection>
      </Card>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  }
};
