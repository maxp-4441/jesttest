import React, { Component } from 'react';
import { View, Text } from 'react-native';
 import firebase from 'firebase';
 import Button  from './components/common/Button';
import Card from './components/common/Card';
import CardSection from './components/common/CardSection';
import Spinner from './components/common/Spinner';
import Header from './components/common/Header'
import LoginForm from './components/LoginForm';

class App extends Component {
 state = { loggedIn: null, user: null };

  componentWillMount() {
    if (!firebase.apps.length) {
     firebase.initializeApp({
        apiKey: 'AIzaSyDLhxi7vaah9Fgv_UO5JEFzP_XuQZ3m1GM',
        authDomain: 'auth-dc11b.firebaseapp.com',
        databaseURL: 'https://auth-dc11b.firebaseio.com',
        projectId: 'auth-dc11b',
        storageBucket: 'auth-dc11b.appspot.com',
        messagingSenderId: '79770221880'
    });
  }
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({ loggedIn: true, user });
      } else {
        this.setState({ loggedIn: false, user: null });
      }
    });
  }

  renderContent() {
    switch (this.state.loggedIn) {
      case true:
      return (
        <Card>
        <CardSection>
          <Text>
            { JSON.stringify(this.state.user.email) }
          </Text>
          </CardSection>
          <CardSection>
          <Button onPress={() => { firebase.auth().signOut(); }}>
            Log Out
          </Button>
          </CardSection>
        </Card>
        );
      case false:
        return <LoginForm />;
      default:
        return <Spinner size="large" />;
    }
  }

  render() {
    return (
      <View style={{ flex: 1}}>
        <Header headerText="Authentication" />   
        {this.renderContent()}
      </View>
    );
  }
}

export default App;
